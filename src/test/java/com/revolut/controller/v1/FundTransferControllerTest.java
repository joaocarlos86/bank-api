package com.revolut.controller.v1;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;

import com.revolut.controller.util.ContextParameterUtils;
import com.revolut.domain.bank.Bank;
import com.revolut.domain.bank.BankAccount;
import com.revolut.domain.bank.dto.FundsTransferDTO;

import io.javalin.http.Context;
import io.javalin.http.util.ContextUtil;

public class FundTransferControllerTest {
  
  private static final String SUBMIT_FUNDS_TRANSFER_URL = "/v1/transfer_funds/";
  private final FundTransferController controller = new FundTransferController();

  @Test
  public void shouldExecuteTransferWithNoErrorsIfValidOperationIsRequested() throws Exception{
    final Bank bank = mock(Bank.class);
    when(bank.getAccount(123)).thenReturn(Optional.of(new BankAccount(123, 200D, "ABC")));
    when(bank.getAccount(456)).thenReturn(Optional.of(new BankAccount(456, 300D, "ABCD")));
    controller.setBank(bank);

    final HttpServletRequest req = mock(HttpServletRequest.class);
    final HttpServletResponse res = mock(HttpServletResponse.class);
    final Context ctx = ContextUtil.init(req, res, SUBMIT_FUNDS_TRANSFER_URL);

    final ContextParameterUtils requestUtils = mock(ContextParameterUtils.class);
    when(requestUtils.getObjectFromBody(FundsTransferDTO.class, ctx)).thenReturn(new FundsTransferDTO(123, 456, 100D));
    controller.setFormUtils(requestUtils);
    
    controller
      .submit()
      .handle(ctx);
  }

}
