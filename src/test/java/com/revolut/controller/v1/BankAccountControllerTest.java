package com.revolut.controller.v1;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;

import com.revolut.domain.bank.Bank;
import com.revolut.domain.bank.BankAccount;
import com.revolut.service.exception.bank.BankAccountNotFoundException;

import io.javalin.http.Context;
import io.javalin.http.util.ContextUtil;

public class BankAccountControllerTest {

  private static final String BANK_ACCOUNT_NUMBER_PARAMETER_NAME = "bankAccountNumber";
  private static final String GET_BANK_ACCOUNT_URL = "/v1/bank/account/:bankAccountNumber";
  private BankAccountController bankAccountController;
  
  @Test
  public void shouldntThrowExceptionIfBankAccountIsFound() throws Exception{
    final Bank bank = mock(Bank.class);
    final int accountNumber = 123456;
    final Optional<BankAccount> bankAccount = Optional.of(new BankAccount(accountNumber, 10D, "ABC"));
    when(bank.getAccount(accountNumber)).thenReturn(bankAccount);
    bankAccountController = new BankAccountController();
    bankAccountController.setBank(bank);
    
    final HttpServletRequest req = mock(HttpServletRequest.class);
    final HttpServletResponse res = mock(HttpServletResponse.class);
    final Map<String, String> pathParams = new HashMap<>();
    pathParams.put(BANK_ACCOUNT_NUMBER_PARAMETER_NAME, "123456");
    Context ctx = ContextUtil.init(req, res, GET_BANK_ACCOUNT_URL, pathParams );

    bankAccountController
      .getBankAccount()
      .handle(ctx);
    
  }
  
  @Test
  public void shouldThrowBankAccountNotFoundExceptionIfBankAccountIsntFound() {
    final Bank bank = mock(Bank.class);
    final int accountNumber = 123456;
    when(bank.getAccount(accountNumber)).thenReturn(Optional.empty());
    bankAccountController = new BankAccountController();
    bankAccountController.setBank(bank);
    
    final HttpServletRequest req = mock(HttpServletRequest.class);
    final HttpServletResponse res = mock(HttpServletResponse.class);
    final Map<String, String> pathParams = new HashMap<>();
    pathParams.put(BANK_ACCOUNT_NUMBER_PARAMETER_NAME, "123456");
    final Context ctx = ContextUtil
        .init(req, res, GET_BANK_ACCOUNT_URL, pathParams);
    
    assertThrows(
        BankAccountNotFoundException.class, 
        () -> bankAccountController
                .getBankAccount()
                .handle(ctx));
  }

}
