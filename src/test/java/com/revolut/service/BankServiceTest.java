package com.revolut.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.revolut.domain.bank.BankAccount;
import com.revolut.service.BankService;
import com.revolut.service.exception.bank.BankTransactionException;
import com.revolut.service.exception.bank.InsufficientFundsException;
import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

@ExtendWith(MockitoExtension.class)
public class BankServiceTest {
  
  private static final int DEFAULT_ROUNDING_MODE = BigDecimal.ROUND_HALF_DOWN;
  private static final int DEFAULT_NUMBER_SCALE = 2;
  private BankService bankService;

  @BeforeEach
  public void setup() {
    bankService = new BankService();
  }
  
  @Test
  public void canTransferFundsBetweenTwoAccounts() {
    final BankAccount source = new BankAccount(1, 10D, "123");
    final BankAccount target = new BankAccount(2, 10D, "124");
    final FundsTransferOperation transferFunds = FundsTransferOperation
        .builder()
        .source(source)
        .target(target)
        .amount(BigDecimal.valueOf(10D))
        .build();
    
    bankService.fundTransfer(transferFunds);
    assertEquals(source.getBalance().doubleValue(), Double.valueOf(0D));
    assertEquals(target.getBalance().doubleValue(), Double.valueOf(20D));
  }
  
  @Test()
  public void cantTransferFundsIfSourceAccountDoesntHaveEnoughFunds() {
    final BankAccount source = new BankAccount(1, 0D, "123");
    final BankAccount target = new BankAccount(2, 10D, "124");
    final FundsTransferOperation transferFunds = FundsTransferOperation
        .builder()
        .source(source)
        .target(target)
        .amount(BigDecimal.valueOf(10D))
        .build();
    
    assertThrows(InsufficientFundsException.class, () -> bankService.fundTransfer(transferFunds));
  }
  
  @Test
  public void cantTransferFundsIfTheBankAccountsAreTheSame() {
    final BankAccount source = new BankAccount(1, 10D, "123");
    final BankAccount target = new BankAccount(1, 10D, "123");
    final FundsTransferOperation transferFunds = FundsTransferOperation
        .builder()
        .source(source)
        .target(target)
        .amount(BigDecimal.valueOf(10D))
        .build();
    
    assertThrows(InvalidTransferFundsException.class, () -> bankService.fundTransfer(transferFunds));
  }
  
  @Test
  public void cantTransferNegativeOrZeroFunds() {
    final BankAccount source = new BankAccount(1, 10D, "123");
    final BankAccount target = new BankAccount(2, 10D, "124");
    final FundsTransferOperation transferFunds = FundsTransferOperation
        .builder()
        .source(source)
        .target(target)
        .amount(BigDecimal.valueOf(-10D))
        .build();
    
    assertThrows(InvalidTransferFundsException.class, () -> bankService.fundTransfer(transferFunds));
  }
  
  @Test
  public void cantTransferIfSourceBankAccountIsNull() {
    final BankAccount source = null;
    final BankAccount target = new BankAccount(2, 10D, "124");
    final FundsTransferOperation transferFunds = FundsTransferOperation
        .builder()
        .source(source)
        .target(target)
        .amount(BigDecimal.valueOf(10D))
        .build();
    
    assertThrows(InvalidTransferFundsException.class, () -> bankService.fundTransfer(transferFunds));
  }
  
  @Test
  public void cantTransferIfTargetBankAccountIsNull() {
    final BankAccount source = new BankAccount(1, 200D, "123");
    final BankAccount target = null;
    final FundsTransferOperation transferFunds = FundsTransferOperation
        .builder()
        .source(source)
        .target(target)
        .amount(BigDecimal.valueOf(10D))
        .build();
    
    assertThrows(InvalidTransferFundsException.class, () -> bankService.fundTransfer(transferFunds));
  }
  
  @Test
  public void canPreserveTransactionsStateIfSomethigGoesWrong() {
    final BankAccount source = mock(BankAccount.class);
    final BankAccount target = mock(BankAccount.class);
    
    when(source.getBalance()).thenReturn(BigDecimal.valueOf(1000D));
    
    when(source.addTransaction(any()))
      .thenThrow(new IllegalStateException());
    
    final FundsTransferOperation transferFunds = FundsTransferOperation
        .builder()
        .source(source)
        .target(target)
        .amount(BigDecimal.valueOf(10D))
        .build();
    
    assertThrows(BankTransactionException.class, () -> bankService.fundTransfer(transferFunds));
    assertTrue(0 == source.getNumberOfTransactions());
    assertTrue(0 == target.getNumberOfTransactions());
    
    final BankAccount anotherSource = mock(BankAccount.class);
    when(anotherSource.getBalance())
      .thenReturn(BigDecimal.valueOf(1000D));
    
    when(anotherSource.addTransaction(any()))
      .thenReturn(true);
    when(target.addTransaction(any()))
      .thenThrow(new IllegalStateException());
    
    final FundsTransferOperation anotherTransferFunds = FundsTransferOperation
        .builder()
        .source(anotherSource)
        .target(target)
        .amount(BigDecimal.valueOf(10D))
        .build();
    
    assertThrows(BankTransactionException.class, () -> bankService.fundTransfer(anotherTransferFunds));
    assertTrue(0 == anotherSource.getNumberOfTransactions());
    assertTrue(0 == target.getNumberOfTransactions());
  }
  
  @Test
  public void allTransactionsAreLoggedInTheAccount() {
    final double initialBalance = 10000D;
    final BankAccount source = new BankAccount(1, initialBalance, "123");
    final BankAccount target = new BankAccount(2, initialBalance, "124");
    
    final int numberOfOperations = 100;
    IntStream.range(0, numberOfOperations).forEach(i -> {
      final FundsTransferOperation transferFunds = FundsTransferOperation
          .builder()
          .source(source)
          .target(target)
          .amount(BigDecimal.valueOf(1D))
          .build();
      
      bankService.fundTransfer(transferFunds);
    });
    
    assertEquals(numberOfOperations, source.getNumberOfTransactions());
  }
  
  @Test
  public void allTransactionsAreConsistentOverComplexAmounts() {
    final double initialBalance = 10000D;
    final BankAccount source = new BankAccount(1, initialBalance, "123");
    final BankAccount target = new BankAccount(2, initialBalance, "124");
    
    AtomicReference<BigDecimal> expectedBalanceForAccount1 = new AtomicReference<>(BigDecimal.valueOf(initialBalance));
    
    final int numberOfOperations = 100;
    IntStream
      .range(0, numberOfOperations)
      .forEach(i -> {
        BigDecimal amount = source.getBalance().multiply(BigDecimal.valueOf(Math.random()));
        
        final FundsTransferOperation transferFunds = FundsTransferOperation
            .builder()
            .source(source)
            .target(target)
            .amount(amount)
            .build();
        
        bankService.fundTransfer(transferFunds);
        expectedBalanceForAccount1.accumulateAndGet(amount, (x,y) -> x.subtract(y));
        
        amount = target.getBalance().multiply(BigDecimal.valueOf(Math.random()));
  
        final FundsTransferOperation transferFunds2 = FundsTransferOperation
            .builder()
            .source(target)
            .target(source)
            .amount(amount)
            .build();
        
        bankService.fundTransfer(transferFunds2);
        expectedBalanceForAccount1.accumulateAndGet(amount, (x,y) -> x.add(y));
      });
    
    final BigDecimal expectedBalance = expectedBalanceForAccount1
        .get()
        .setScale(DEFAULT_NUMBER_SCALE, DEFAULT_ROUNDING_MODE);
    
    assertEquals(expectedBalance, source.getBalance());
    assertEquals(numberOfOperations * 2, source.getNumberOfTransactions());
  }
  
}
