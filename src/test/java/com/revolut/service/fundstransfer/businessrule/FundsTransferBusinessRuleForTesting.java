package com.revolut.service.fundstransfer.businessrule;

import com.revolut.service.operation.fundstransfer.FundsTransferOperation;
import com.revolut.service.operation.fundstransfer.businessrule.AbstractTransferFundsBusinessRule;

public class FundsTransferBusinessRuleForTesting extends AbstractTransferFundsBusinessRule{

  @Override
  public void check(FundsTransferOperation parameter) {
  }

}
