package com.revolut.service.fundstransfer.businessrule;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.revolut.domain.bank.BankAccount;
import com.revolut.service.exception.bank.InsufficientFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;
import com.revolut.service.operation.fundstransfer.businessrule.ShouldHaveEnoughFundsToTransfer;

@ExtendWith(MockitoExtension.class)
public class ShouldHaveEnoughFundsToTransferTest {

  @Mock
  private BankAccount bank;
  
  @Test
  public void shouldThrowExceptionIfSourceBankAccountDoesntHaveEnoughtBalance() {
    ShouldHaveEnoughFundsToTransfer rule = new ShouldHaveEnoughFundsToTransfer();
    when(bank.getBalance()).thenReturn(BigDecimal.valueOf(100D));
    
    final FundsTransferOperation operation = FundsTransferOperation
        .builder()
        .source(bank)
        .amount(BigDecimal.valueOf(101D))
        .build();
    
    assertThrows(InsufficientFundsException.class, () -> rule.check(operation));
  }
  
  @Test
  public void shouldNotThrowExceptionIfSourceBankAccountDoesntHaveEnoughtBalance() {
    ShouldHaveEnoughFundsToTransfer rule = new ShouldHaveEnoughFundsToTransfer();
    when(bank.getBalance()).thenReturn(BigDecimal.valueOf(1000D));
    
    final FundsTransferOperation operation = FundsTransferOperation
        .builder()
        .source(bank)
        .amount(BigDecimal.valueOf(101D))
        .build();
    
    rule.check(operation);
  }

}
