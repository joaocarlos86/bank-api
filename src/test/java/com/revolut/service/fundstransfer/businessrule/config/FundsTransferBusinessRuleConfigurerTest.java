package com.revolut.service.fundstransfer.businessrule.config;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.revolut.service.operation.fundstransfer.businessrule.AbstractTransferFundsBusinessRule;
import com.revolut.service.operation.fundstransfer.businessrule.config.FundsTransferBusinessRuleConfigurer;

public class FundsTransferBusinessRuleConfigurerTest {

  @Test
  public void getBusinessRulesShouldReturnTheExpectedElementsRegardlessAnySideModification() {
    final FundsTransferBusinessRuleConfigurer configurer = new FundsTransferBusinessRuleConfigurer();
    final List<AbstractTransferFundsBusinessRule> businessRules = configurer.getBusinessRules();
    assertTrue(5 == businessRules.size());
    businessRules.remove(0);
    assertTrue(4 == businessRules.size());
    
    final List<AbstractTransferFundsBusinessRule> anotherBusinessRules = configurer.getBusinessRules();
    assertTrue(5 == anotherBusinessRules.size());
  }

}
