package com.revolut.service.fundstransfer.businessrule;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.revolut.domain.bank.BankAccount;
import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;
import com.revolut.service.operation.fundstransfer.businessrule.ShouldSpecifySourceBankAccount;

@ExtendWith(MockitoExtension.class)
public class ShouldSpecifySourceBankAccountTest {

  private final ShouldSpecifySourceBankAccount rule = new ShouldSpecifySourceBankAccount();

  @Mock
  private FundsTransferOperation operation;

  @Test
  public void shouldThrowExceptionIfNoSourceBankAccountIsSpecified() {
    when(operation.getSource()).thenReturn(null);
    assertThrows(InvalidTransferFundsException.class, () -> rule.check(operation));
  }
  
  @Test
  public void shouldNotExceptionIfSourceBankAccountIsSpecified() {
    when(operation.getSource()).thenReturn(new BankAccount(0, 0D, ""));
    rule.check(operation);
  }

  @Test
  public void shouldFail() {
    int sum = 1 + 2;
    assertEquals(3, sum);
  }

}
