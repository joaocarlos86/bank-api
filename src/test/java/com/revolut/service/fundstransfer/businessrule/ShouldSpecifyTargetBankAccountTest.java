package com.revolut.service.fundstransfer.businessrule;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.revolut.domain.bank.BankAccount;
import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;
import com.revolut.service.operation.fundstransfer.businessrule.ShouldSpecifyTargetBankAccount;

@ExtendWith(MockitoExtension.class)
public class ShouldSpecifyTargetBankAccountTest {

  private final ShouldSpecifyTargetBankAccount rule = new ShouldSpecifyTargetBankAccount();
  @Mock
  private FundsTransferOperation operation;
  
  @Test
  public void shouldThrowExceptionIfNoTargetBankAccountIsSpecified() {
    when(operation.getTarget()).thenReturn(null);
    assertThrows(InvalidTransferFundsException.class, () -> rule.check(operation));
  }
  
  @Test
  public void shouldNotExceptionIfTargetBankAccountIsSpecified() {
    when(operation.getTarget()).thenReturn(new BankAccount(0, 0D, ""));
    rule.check(operation);
  }
}
