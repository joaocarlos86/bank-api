package com.revolut.service.fundstransfer.businessrule;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.revolut.service.operation.fundstransfer.FundsTransferOperation;
import com.revolut.service.operation.fundstransfer.businessrule.AbstractTransferFundsBusinessRule;

public class AbstractTransferFundsBusinessRuleTest {

  private final AbstractTransferFundsBusinessRule transferFundsBusinessRule = new FundsTransferBusinessRuleForTesting();
  
  @Test
  public void shouldApplyBusinessRuleForTransferFundsOperation() {
    assertTrue(transferFundsBusinessRule.supports(FundsTransferOperation.class));
  }
  
  @Test
  public void shouldNotApplyBusinessRuleForTransferFundsOperation() {
    assertFalse(transferFundsBusinessRule.supports(String.class));
  }

}
