package com.revolut.service.fundstransfer.businessrule;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.revolut.domain.bank.BankAccount;
import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;
import com.revolut.service.operation.fundstransfer.businessrule.ShouldSpecifyValidTargetAccount;

@ExtendWith(MockitoExtension.class)
public class ShouldSpecifyValidTargetAccountTest {

  private final ShouldSpecifyValidTargetAccount rule = new ShouldSpecifyValidTargetAccount();
  @Mock
  private FundsTransferOperation operation;
  @Mock
  private BankAccount source;
  @Mock
  private BankAccount target;
  
  @Test
  public void shouldThrowExceptionIfSourceAndTargetBankAccountsAreTheSame() {
    when(operation.getSource()).thenReturn(source);
    when(operation.getTarget()).thenReturn(source);
    assertThrows(InvalidTransferFundsException.class, () -> rule.check(operation));
  }
  
  @Test
  public void shouldNotExceptionIfTargetBankAccountIsSpecified() {
    when(operation.getSource()).thenReturn(source);
    rule.check(operation);
  }
}
