package com.revolut.service.fundstransfer.businessrule;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;
import com.revolut.service.operation.fundstransfer.businessrule.AmountShouldBeOverMinimunLimit;

public class AmountShouldBeOverMinimunLimitTest {

  @Test
  public void shouldThrowExceptionIfTranferAmountIsLessOrEqualTheMinimunSpecified() {
    final AmountShouldBeOverMinimunLimit rule = new AmountShouldBeOverMinimunLimit();
    final FundsTransferOperation parameterWithNegativeTransferAmount = FundsTransferOperation
        .builder()
        .amount(BigDecimal.valueOf(-1D))
        .build();
    
    assertThrows(InvalidTransferFundsException.class, () -> rule.check(parameterWithNegativeTransferAmount));
    
    final FundsTransferOperation operationWithZeroTransferAmount = FundsTransferOperation
        .builder()
        .amount(BigDecimal.valueOf(0D))
        .build();
    
    assertThrows(InvalidTransferFundsException.class, () -> rule.check(operationWithZeroTransferAmount));
  }
  
  @Test
  public void shouldNotThrowExceptionIfTranferAmountIsLessThanSpecified() {
    final AmountShouldBeOverMinimunLimit rule = new AmountShouldBeOverMinimunLimit();
    final FundsTransferOperation parameter = FundsTransferOperation
        .builder()
        .amount(BigDecimal.valueOf(10))
        .build();
    
    rule.check(parameter);
  }  

}
