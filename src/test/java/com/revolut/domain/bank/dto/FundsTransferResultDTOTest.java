package com.revolut.domain.bank.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import com.revolut.domain.bank.BankAccount;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

public class FundsTransferResultDTOTest {

  @Test
  public void shouldCreateFundsTransaferResultFromFundsTransfer() {
    final FundsTransferOperation operation = FundsTransferOperation
        .builder()
        .amount(BigDecimal.TEN)
        .date(LocalDateTime.now())
        .source(new BankAccount(123, 100D, ""))
        .target(new BankAccount(456, 100D, "ASD"))
        .build();
    
    FundsTransferResultDTO fundsTransferResult = new FundsTransferResultDTO(operation);
    
    assertEquals(operation.getAmount(), fundsTransferResult.getAmount());
    assertEquals(new BankAccountDTO(operation.getSource()), fundsTransferResult.getSource());
    assertEquals(new BankAccountDTO(operation.getTarget()), fundsTransferResult.getTarget());
  }

}
