package com.revolut.domain.bank.dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.revolut.domain.bank.BankAccount;

public class BankAccountDTOTest {

  @Test
  public void shouldCreateBankAccountDTOFromBankAccount() {
    final int accountNumber = 123;
    final double initialBalance = 100D;
    final String iban = "";
    
    BankAccountDTO bankAccount = new BankAccountDTO(new BankAccount(accountNumber, initialBalance, iban));
    assertEquals(accountNumber, bankAccount.getAccountNumber());
    assertEquals(initialBalance, bankAccount.getBalance());
    assertEquals(iban, bankAccount.getIban());
    assertEquals(0, bankAccount.getNumberOfTransactions());
  }

}
