package com.revolut.domain.bank;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

public class BankAccountTest {

  @Test
  public void shouldCreateBankAccountWithZeroInitialBalanceIfInitialBalanceIsntProvided() {
    final BankAccount account = new BankAccount(123, null, "123");
    
    assertEquals(BigDecimal.valueOf(0D), account.getBalance());
  }

  @Test
  public void shouldFailToCreateBankAccountIfAccountNumberIsntProvided() {
    assertThrows(IllegalArgumentException.class, () -> new BankAccount(null, null, "123"));
  }

  @Test
  public void shouldFailToCreateBankAccountIfIBANIsntProvided() {
    assertThrows(IllegalArgumentException.class, () -> new BankAccount(123, null, null));
  }
  
  @Test
  public void shouldRoundInitialBalanceIfInitialBalanceWithMoreThanTwoDecimalNumbersIsProvided() {
    final double initialBalance = 1234.56789D;
    final double expectedInitialBalance = 1234.57;
    final BankAccount account = new BankAccount(123, initialBalance, "123");
    
    assertEquals(BigDecimal.valueOf(expectedInitialBalance), account.getBalance());

    final double anotherInitialBalance = 1234.54489D;
    final double anotherExpectedInitialBalance = 1234.54;
    final BankAccount anotherAccount = new BankAccount(1234, anotherInitialBalance, "1234");
    assertEquals(BigDecimal.valueOf(anotherExpectedInitialBalance), anotherAccount.getBalance());
  }
  
}
