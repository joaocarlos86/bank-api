package com.revolut.domain.bank;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

public class BankTest {

  @Test
  public void canAddBankAccount() {
    final Bank bank = Bank.getBank();
    final Integer accountNumber = 123456;
    final BankAccount account = new BankAccount(accountNumber, 0D, "123");
    bank.addAccount(account);
    final Optional<BankAccount> bankAccount = bank.getAccount(accountNumber);
    assertTrue(bankAccount.isPresent());
    assertEquals(account, bankAccount.get());
  }

  @Test
  public void canAddManyBankAccounts() {
    final Bank bank = Bank.getBank();
    final int numberOfAccountsToAdd = 10;
    IntStream
      .range(0, numberOfAccountsToAdd)
      .forEach(accountNumber -> {
        final BankAccount account = new BankAccount(accountNumber+10, 0D, "123");
        bank.addAccount(account);
      });
    
    IntStream
    .range(0, numberOfAccountsToAdd)
    .forEach(accountNumber -> {
      assertTrue(bank.getAccount(accountNumber+10).isPresent());
    });
  }
  
  @Test
  public void shouldThrowExceptionWhenAddBankAccountWithConflictingNumber() {
    final Bank bank = Bank.getBank();
    final Integer accountNumber = 1;
    final BankAccount account = new BankAccount(accountNumber, 0D, "123");
    bank.addAccount(account);
    
    final BankAccount newAccount = new BankAccount(accountNumber, 1D, "1234");
    assertThrows(IllegalArgumentException.class, () -> bank.addAccount(newAccount));
  }
  
}
