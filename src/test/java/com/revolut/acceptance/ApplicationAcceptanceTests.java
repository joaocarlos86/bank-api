package com.revolut.acceptance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.Application;
import com.revolut.acceptance.utils.RequestUtils;
import com.revolut.acceptance.utils.ResponseUtils;
import com.revolut.domain.bank.dto.BankAccountDTO;
import com.revolut.domain.bank.dto.FundsTransferDTO;
import com.revolut.domain.bank.dto.FundsTransferResultDTO;

@TestInstance(Lifecycle.PER_CLASS)
public class ApplicationAcceptanceTests {
  
  private static final String GET_BANK_ACCOUNT_URL = "http://localhost:8081/v1/bank/account/%s";
  private static final String FUND_TRANSFER_URL = "http://localhost:8081/v1/transfer_funds/";
  private final Application application = new Application();
  private final ResponseUtils responseUtils = new ResponseUtils();
  private final RequestUtils requestUtils = new RequestUtils();
  
  @BeforeAll
  public void setup() {
    application.start(8081);
  }
  
  @AfterAll
  public void tearDown() {
    application.stop();
  }
  
  @Test
  public void givenValidFundsTransferDTOThenAnUserCanGetSubmitFundTransfer() throws Exception {
    final FundsTransferDTO fundsTransfer = new FundsTransferDTO(1234, 4567, 100D);
    final URL url = new URL(FUND_TRANSFER_URL);
    final HttpURLConnection con = requestUtils.createPostRequestToUrlUsingObjAsBody(fundsTransfer, url);
    
    int status = con.getResponseCode();
    assertTrue(status == 200);
    StringBuffer content = responseUtils.getResponseBody(con);
    
    final FundsTransferResultDTO operation = new ObjectMapper()
        .readValue(content.toString(), FundsTransferResultDTO.class);
    
    assertNotNull(operation);
  }
  
  @Test
  public void givenASuccessfulFundTransferThenTheBalanceOfAccountsShouldChangeAccordly() throws Exception {
    final int sourceBankAccountNumber = 123;
    final int targetBankAccountNumber = 456;
    final BankAccountDTO sourceBankAccount = getBankAccount(sourceBankAccountNumber);
    final BankAccountDTO targetBankAccount = getBankAccount(targetBankAccountNumber);

    final Double transactionAmount = 100D;
    
    final FundsTransferDTO fundsTransfer = new FundsTransferDTO(sourceBankAccountNumber, 
        targetBankAccountNumber, transactionAmount);
    final URL url = new URL(FUND_TRANSFER_URL);
    final HttpURLConnection con = requestUtils.createPostRequestToUrlUsingObjAsBody(fundsTransfer, url);
    
    int status = con.getResponseCode();
    assertTrue(status == 200);
    StringBuffer content = responseUtils.getResponseBody(con);
    
    final FundsTransferResultDTO operation = new ObjectMapper()
        .readValue(content.toString(), FundsTransferResultDTO.class);
    
    assertNotNull(operation);
    
    final BankAccountDTO sourceBankAccountAfterOperation = getBankAccount(sourceBankAccountNumber);
    final BankAccountDTO targetBankAccountAfterOperation = getBankAccount(targetBankAccountNumber);
    
    assertEquals(sourceBankAccount.getBalance() - transactionAmount, sourceBankAccountAfterOperation.getBalance());
    assertEquals(targetBankAccount.getBalance() + transactionAmount, targetBankAccountAfterOperation.getBalance());
  }
  
  @Test
  public void givenAnUnsuccessfulFundTransferThenTheBalanceOfAccountsShouldntChange() throws Exception {
    final int sourceBankAccountNumber = 123;
    final int targetBankAccountNumber = 456;
    final BankAccountDTO sourceBankAccount = getBankAccount(sourceBankAccountNumber);
    final BankAccountDTO targetBankAccount = getBankAccount(targetBankAccountNumber);

    final Double transactionAmount = -100D;
    
    final FundsTransferDTO fundsTransfer = new FundsTransferDTO(sourceBankAccountNumber, 
        targetBankAccountNumber, transactionAmount);
    final URL url = new URL(FUND_TRANSFER_URL);
    final HttpURLConnection con = requestUtils.createPostRequestToUrlUsingObjAsBody(fundsTransfer, url);
    
    int status = con.getResponseCode();
    assertTrue(status == 400);
    
    final BankAccountDTO sourceBankAccountAfterOperation = getBankAccount(sourceBankAccountNumber);
    final BankAccountDTO targetBankAccountAfterOperation = getBankAccount(targetBankAccountNumber);
    
    assertEquals(sourceBankAccount.getBalance(), sourceBankAccountAfterOperation.getBalance());
    assertEquals(targetBankAccount.getBalance(), targetBankAccountAfterOperation.getBalance());
  }

  private BankAccountDTO getBankAccount(Integer bankAccountNumber) throws Exception {
    final URL url = new URL(String.format(GET_BANK_ACCOUNT_URL, bankAccountNumber));
    final HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("GET");
    
    final StringBuffer content = responseUtils.getResponseBody(con);
    
    return new ObjectMapper()
        .readValue(content.toString(), BankAccountDTO.class);
  }
  
  @Test
  public void givenValidBankAccountNumberThenAnUserCanGetBankAccountDetails() throws Exception {
    final URL url = new URL(String.format(GET_BANK_ACCOUNT_URL, 10));
    final HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("GET");
    
    int status = con.getResponseCode();
    StringBuffer content = responseUtils.getResponseBody(con);
    assertTrue(status == 200);
    
    BankAccountDTO bankAccount = new ObjectMapper()
        .readValue(content.toString(), BankAccountDTO.class);
    
    assertNotNull(bankAccount);
    assertEquals(10, bankAccount.getAccountNumber());
  }
  
  @Test
  public void givenNonExistingBankAccountNumberThenAnUserCantGetBankAccountDetails() throws Exception {
    final URL url = new URL(String.format(GET_BANK_ACCOUNT_URL, -1));
    final HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("GET");
    
    int status = con.getResponseCode();
    assertTrue(status == 404);
  }
  
  @Test
  public void givenInvalidBankAccountNumberThenAnUserCantGetBankAccountDetails() throws Exception {
    final URL url = new URL(String.format(GET_BANK_ACCOUNT_URL, "ABC"));
    final HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("GET");
    
    int status = con.getResponseCode();
    assertTrue(status == 500);
  }
  
  @Test
  public void givenNoBankAccountNumberThenAnUserCantGetBankAccountDetails() throws Exception {
    final URL url = new URL(String.format(GET_BANK_ACCOUNT_URL, ""));
    final HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("GET");
    
    int status = con.getResponseCode();
    assertEquals(404, status);
  }
  
}
