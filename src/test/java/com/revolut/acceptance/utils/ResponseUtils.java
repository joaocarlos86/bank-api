package com.revolut.acceptance.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class ResponseUtils {

  public StringBuffer getResponseBody(HttpURLConnection con) throws IOException {
    try(final InputStream inputStream = con.getInputStream()){
      try(final InputStreamReader inputStreamReader = new InputStreamReader(inputStream)){
        try(final BufferedReader reader = new BufferedReader(inputStreamReader)){
          StringBuffer content = new StringBuffer();
          String inputLine = "";
          while ((inputLine = reader.readLine()) != null) {
            content.append(inputLine);
          }
          reader.close();
          inputStream.close();
          return content;
        }
      }
    }
  }
}
