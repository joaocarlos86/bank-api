package com.revolut.acceptance.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.domain.bank.dto.FundsTransferDTO;

public class RequestUtils {
  public HttpURLConnection createPostRequestToUrlUsingObjAsBody(final FundsTransferDTO fundsTransfer, final URL url)
      throws IOException, ProtocolException, JsonProcessingException {
    final HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("POST");
    con.setDoOutput(true);
    
    try(final DataOutputStream stream = new DataOutputStream(con.getOutputStream())){
      final String body = new ObjectMapper()
          .writeValueAsString(fundsTransfer);
      stream.writeBytes(body);
      stream.flush();
    }
    
    return con;
  }
}
