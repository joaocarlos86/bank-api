package com.revolut.domain.enums;

public enum BankComplexOperationType {
  FUNDS_TRANSFER("Funds transfer between accounts");
  
  private String description;

  private BankComplexOperationType(final String description) {
    this.description = description;
  }
  
  public String getDescription() {
    return description;
  }
}
