package com.revolut.domain.enums;

public enum BankSimpleOperationType {
  CREDIT(1), DEBIT(-1);
  
  private Integer multiplicator;
  
  private BankSimpleOperationType(final Integer multiplicator) {
    this.multiplicator = multiplicator;
  }
  
  public Integer getMultiplicator() {
    return multiplicator;
  }

}
