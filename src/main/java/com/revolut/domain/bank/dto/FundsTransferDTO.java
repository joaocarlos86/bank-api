package com.revolut.domain.bank.dto;

import lombok.Data;

@Data
public class FundsTransferDTO {
  private Integer sourceBankAccountNumber;
  private Integer targetBankAccountNumber;
  private Double amount;
  
  public FundsTransferDTO() {
    
  }

  public FundsTransferDTO(Integer sourceBankAccountNumber, Integer targetBankAccountNumber, Double amount) {
    super();
    this.sourceBankAccountNumber = sourceBankAccountNumber;
    this.targetBankAccountNumber = targetBankAccountNumber;
    this.amount = amount;
  }
  
}
