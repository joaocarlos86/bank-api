package com.revolut.domain.bank.dto;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.Date;

import com.revolut.domain.enums.BankComplexOperationType;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

import lombok.Data;

@Data
public class FundsTransferResultDTO {
  private final BankComplexOperationType type = BankComplexOperationType.FUNDS_TRANSFER; 
  private BankAccountDTO source;
  private BankAccountDTO target;
  private BigDecimal amount;
  private Date date;
  
  public FundsTransferResultDTO() {
    
  }
  
  public FundsTransferResultDTO(final FundsTransferOperation operation) {
    this.source = new BankAccountDTO(operation.getSource());
    this.target = new BankAccountDTO(operation.getTarget());
    this.amount = operation.getAmount();
    this.date = new Date(
        operation
          .getDate()
          .atZone(ZoneId.systemDefault())
          .toInstant()
          .toEpochMilli());
  }
}
