package com.revolut.domain.bank.dto;

import com.revolut.domain.bank.BankAccount;

import lombok.Data;

@Data
public class BankAccountDTO {
  private Integer accountNumber;
  private String iban;
  private Double balance;
  private Integer numberOfTransactions;
  
  public BankAccountDTO() {
    
  }
  
  public BankAccountDTO(final BankAccount bankAccount) {
    this.accountNumber = bankAccount.getAccountNumber();
    this.iban = bankAccount.getIban();
    this.balance = bankAccount.getBalance().doubleValue();
    this.numberOfTransactions = bankAccount.getNumberOfTransactions();
  }
}
