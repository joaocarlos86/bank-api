package com.revolut.domain.bank;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class Bank {
  private String name;
  private Map<Integer, BankAccount> accounts = new ConcurrentHashMap<>();
  
  private Bank(final String name) {
    this.name = name;
  }
  
  public String getName() {
    return name;
  }
  
  public void addAccount(final BankAccount bankAccount) {
    final BankAccount putIfAbsent = accounts.putIfAbsent(bankAccount.getAccountNumber(), bankAccount);
    if(putIfAbsent != null) {
      throw new IllegalArgumentException("The supplied bank account is already in the datastore");
    }
  }
  
  public Optional<BankAccount> getAccount(final Integer accountNumber) {
    return Optional.ofNullable(accounts.get(accountNumber));
  }
  
  private static class BankHolder {
    static final Bank BANK = new Bank("Test bank");
  }
  
  public static Bank getBank() {
    return BankHolder.BANK;
  }
}
