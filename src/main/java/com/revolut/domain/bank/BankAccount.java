package com.revolut.domain.bank;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.revolut.domain.operation.BankOperation;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(of = {"iban", "accountNumber"})
public class BankAccount {
  private static final double DEFAULT_INITIAL_BALANCE = 0D;
  private static final int DEFAULT_ROUNDING_MODE = BigDecimal.ROUND_HALF_DOWN;
  private static final int DEFAULT_NUMBER_SCALE = 2;
  @Getter
  private Integer accountNumber;
  @Getter
  private String iban;
  private final Double initialBalance;
  private final Queue<BankOperation> transactions = new ConcurrentLinkedQueue<>();
  
  public BankAccount() {
    initialBalance = DEFAULT_INITIAL_BALANCE;
  }
  
  public BankAccount(final Integer accountNumber, final Double initialBalance, final String iban) {
    this.accountNumber = Optional
        .ofNullable(accountNumber)
        .orElseThrow(() -> new IllegalArgumentException("No bank account supplied"));
    
    this.initialBalance = BigDecimal
        .valueOf(
            Optional
            .ofNullable(initialBalance)
            .orElse(DEFAULT_INITIAL_BALANCE))
        .setScale(DEFAULT_NUMBER_SCALE, DEFAULT_ROUNDING_MODE)
        .doubleValue();
    
    this.iban = Optional
        .ofNullable(iban)
        .orElseThrow(() -> new IllegalArgumentException("No IBAN supplied"));
  }
  
  public BigDecimal getBalance() {
    if(transactions == null || transactions.size() == 0) {
      return BigDecimal.valueOf(initialBalance);
    }
    
    return transactions
        .stream()
        .map(transaction -> transaction.getAmount())
        .reduce((transactionAmount, anotherTransactionAmount) -> transactionAmount.add(anotherTransactionAmount))
        .orElse(BigDecimal.ZERO)
        .add(BigDecimal.valueOf(initialBalance))
        .setScale(DEFAULT_NUMBER_SCALE, DEFAULT_ROUNDING_MODE);
  }

  public boolean addTransaction(final BankOperation bankOperation) {
    return transactions.add(bankOperation);
  }
  
  public Integer getNumberOfTransactions() {
    return transactions.size();
  }

  public void removeTransaction(BankOperation moneyOut) {
    if(transactions.contains(moneyOut)) {
      transactions.remove(moneyOut);
    }
  }
}
