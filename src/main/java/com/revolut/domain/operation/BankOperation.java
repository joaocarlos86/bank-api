package com.revolut.domain.operation;

import java.math.BigDecimal;

import com.revolut.domain.enums.BankSimpleOperationType;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class BankOperation {
  private BankSimpleOperationType type;
  private FundsTransferOperation details;
  private final BigDecimal amount;
  
  @Builder
  private BankOperation(final BankSimpleOperationType type, final FundsTransferOperation details) {
    this.type = type;
    this.details = details;
    if (BankSimpleOperationType.DEBIT.equals(type)) {
      this.amount = details
          .getAmount()
          .multiply(BigDecimal.valueOf(-1));
    } else {
      this.amount = details.getAmount();
    }
  }
}
