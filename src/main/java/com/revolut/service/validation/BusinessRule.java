package com.revolut.service.validation;

import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

public interface BusinessRule {
  public Boolean supports(Class<?> type);
  public void check(FundsTransferOperation parameter);
}
