package com.revolut.service.exception.bank;

public class BankAccountNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 4089900642771339434L;

  public BankAccountNotFoundException(final Integer accountNumber) {
    super(String.format("Account %s doens't exist", accountNumber));
  }
  
}
