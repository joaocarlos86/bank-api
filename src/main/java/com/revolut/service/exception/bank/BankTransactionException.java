package com.revolut.service.exception.bank;

public class BankTransactionException extends RuntimeException {

  public BankTransactionException(String message, RuntimeException e) {
    super(message, e);
  }

  public BankTransactionException(String message) {
    super(message);
  }

  private static final long serialVersionUID = 6970363755871717556L;

}
