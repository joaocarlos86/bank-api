package com.revolut.service.exception.bank;

public class InvalidTransferFundsException extends RuntimeException {

  private static final long serialVersionUID = -9162794224169966095L;

  public InvalidTransferFundsException(final String message) {
    super(message);
  }

}
