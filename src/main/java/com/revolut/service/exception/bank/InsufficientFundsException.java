package com.revolut.service.exception.bank;

import com.revolut.domain.bank.BankAccount;

public class InsufficientFundsException extends RuntimeException {

  private static final long serialVersionUID = 6754609822293700550L;
  
  public InsufficientFundsException(BankAccount from) {
    super(String.format("The operation can't be completed because there isn't enough funds %s", 
        from.toString()));
  }

}
