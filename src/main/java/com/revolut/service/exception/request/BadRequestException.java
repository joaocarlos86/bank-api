package com.revolut.service.exception.request;

public class BadRequestException extends RuntimeException {

  private static final long serialVersionUID = 437588337642749784L;

  public BadRequestException(final String message) {
    super(message);
  }
  
}
