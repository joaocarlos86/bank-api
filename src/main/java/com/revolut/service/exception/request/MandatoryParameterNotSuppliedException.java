package com.revolut.service.exception.request;

public class MandatoryParameterNotSuppliedException extends BadRequestException {

  private static final long serialVersionUID = -7790452698988190228L;

  public MandatoryParameterNotSuppliedException(final String field) {
    super(String.format("Mandatory parameter %s not supplied", field));
  }
}
