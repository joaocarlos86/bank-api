package com.revolut.service.operation.fundstransfer;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import com.revolut.domain.bank.Bank;
import com.revolut.domain.bank.BankAccount;
import com.revolut.domain.enums.BankComplexOperationType;
import com.revolut.service.exception.bank.BankAccountNotFoundException;
import com.revolut.service.operation.fundstransfer.businessrule.config.FundsTransferBusinessRuleConfigurer;

import lombok.Builder;
import lombok.Getter;


@Getter
public class FundsTransferOperation {
  
  private final BankComplexOperationType type = BankComplexOperationType.FUNDS_TRANSFER; 
  private BankAccount source;
  private BankAccount target;
  private BigDecimal amount;
  private LocalDateTime date;

  /**
   * TODO add test case for creating transferFundOperation with and without dates.
   * @param source
   * @param target
   * @param amount
   * @param date
   */
  @Builder
  private FundsTransferOperation(final BankAccount source, final BankAccount target, final BigDecimal amount, final LocalDateTime date) {
    this.source = source;
    this.target = target;
    this.amount = amount;
    this.date = Optional
        .ofNullable(date)
        .orElse(LocalDateTime.now());
  }
  
  @Builder
  private FundsTransferOperation(final Integer sourceAccountNumber, final Integer targetAccountNumber, final Double amount, final LocalDateTime date) {
    this(
        Bank.getBank().getAccount(sourceAccountNumber).orElseThrow(() -> new BankAccountNotFoundException(sourceAccountNumber)),
        Bank.getBank().getAccount(targetAccountNumber).orElseThrow(() -> new BankAccountNotFoundException(targetAccountNumber)),
        BigDecimal.valueOf(amount),
        LocalDateTime.now()
    );
  }
  
  public Boolean isValid() {
    new FundsTransferBusinessRuleConfigurer()
      .getBusinessRules()
      .stream()
      .forEach(businessRule -> businessRule.check(this));
    
    return true;
  }
}
