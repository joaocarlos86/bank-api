package com.revolut.service.operation.fundstransfer.businessrule;

import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

public class ShouldSpecifyTargetBankAccount extends AbstractTransferFundsBusinessRule {

  @Override
  public void check(final FundsTransferOperation operation) {
    if(operation.getTarget() == null) {
      throw new InvalidTransferFundsException("The target bank account wasn't specified");
    }
  }

}
