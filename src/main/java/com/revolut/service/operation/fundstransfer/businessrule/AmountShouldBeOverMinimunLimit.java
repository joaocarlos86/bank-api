package com.revolut.service.operation.fundstransfer.businessrule;

import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

public class AmountShouldBeOverMinimunLimit extends AbstractTransferFundsBusinessRule {
  private static final double MINIMUM_VALUE = 0D;
  
  @Override
  public void check(final FundsTransferOperation operaion) {
    if(operaion.getAmount().doubleValue() <= MINIMUM_VALUE) {
      throw new InvalidTransferFundsException("The amount should be bigger than the minimun value");
    }
  }

}
