package com.revolut.service.operation.fundstransfer.businessrule;

import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

public class ShouldSpecifyValidTargetAccount extends AbstractTransferFundsBusinessRule {

  @Override
  public void check(final FundsTransferOperation operation) {
    if(operation.getSource().equals(operation.getTarget())) {
      throw new InvalidTransferFundsException("Invalid target account");
    }
  }

}
