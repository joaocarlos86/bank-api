package com.revolut.service.operation.fundstransfer.businessrule.config;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.revolut.service.operation.fundstransfer.businessrule.AbstractTransferFundsBusinessRule;
import com.revolut.service.operation.fundstransfer.businessrule.AmountShouldBeOverMinimunLimit;
import com.revolut.service.operation.fundstransfer.businessrule.ShouldHaveEnoughFundsToTransfer;
import com.revolut.service.operation.fundstransfer.businessrule.ShouldSpecifySourceBankAccount;
import com.revolut.service.operation.fundstransfer.businessrule.ShouldSpecifyTargetBankAccount;
import com.revolut.service.operation.fundstransfer.businessrule.ShouldSpecifyValidTargetAccount;

public class FundsTransferBusinessRuleConfigurer {
  final List<AbstractTransferFundsBusinessRule> businessRules = new LinkedList<>();

  public FundsTransferBusinessRuleConfigurer() {
    businessRules.add(new ShouldSpecifySourceBankAccount());
    businessRules.add(new ShouldSpecifyTargetBankAccount());
    businessRules.add(new ShouldSpecifyValidTargetAccount());
    businessRules.add(new ShouldHaveEnoughFundsToTransfer());
    businessRules.add(new AmountShouldBeOverMinimunLimit());
  }
  
  public List<AbstractTransferFundsBusinessRule> getBusinessRules() {
    return businessRules
        .stream()
        .collect(Collectors.toList());
  }
}
