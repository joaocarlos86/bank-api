package com.revolut.service.operation.fundstransfer.businessrule;

import com.revolut.service.exception.bank.InsufficientFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

public class ShouldHaveEnoughFundsToTransfer extends AbstractTransferFundsBusinessRule {
  private static final double MINIMUM_BALANCE = 0D;

  @Override
  public void check(final FundsTransferOperation operation) {
    if(operation.getSource().getBalance().subtract(operation.getAmount()).doubleValue() < MINIMUM_BALANCE) {
      throw new InsufficientFundsException(operation.getSource());
    }
  }
  
}
