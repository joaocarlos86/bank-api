package com.revolut.service.operation.fundstransfer.businessrule;

import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

public class ShouldSpecifySourceBankAccount extends AbstractTransferFundsBusinessRule {

  @Override
  public void check(final FundsTransferOperation operation) {
    if(operation.getSource() == null) {
      throw new InvalidTransferFundsException("The source bank account wasn't specified");
    }
  }

}
