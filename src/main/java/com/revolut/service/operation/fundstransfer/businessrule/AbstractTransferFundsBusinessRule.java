package com.revolut.service.operation.fundstransfer.businessrule;

import com.revolut.service.operation.fundstransfer.FundsTransferOperation;
import com.revolut.service.validation.BusinessRule;

public abstract class AbstractTransferFundsBusinessRule implements BusinessRule {
  
  @Override
  public Boolean supports(Class<?> type) {
    return type.isAssignableFrom(FundsTransferOperation.class);
  }
  
}
