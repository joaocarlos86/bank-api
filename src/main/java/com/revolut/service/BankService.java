package com.revolut.service;

import com.revolut.domain.bank.BankAccount;
import com.revolut.domain.enums.BankSimpleOperationType;
import com.revolut.domain.operation.BankOperation;
import com.revolut.service.exception.bank.BankTransactionException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BankService {
  public void fundTransfer(final FundsTransferOperation transferFunds) {
    if(transferFunds.isValid()) {
      final BankAccount from = transferFunds.getSource();
      final BankAccount to = transferFunds.getTarget();

      final BankOperation moneyOut = BankOperation
          .builder()
          .type(BankSimpleOperationType.DEBIT)
          .details(transferFunds)
          .build();
      
      if(addTransactionToBankAccount(from, moneyOut)) {
        
        final BankOperation moneyIn = BankOperation
            .builder()
            .type(BankSimpleOperationType.CREDIT)
            .details(transferFunds)
            .build();
      
        if(addTransactionToBankAccount(to, moneyIn) == false) {
          final BankOperation revert = BankOperation
              .builder()
              .type(BankSimpleOperationType.CREDIT)
              .details(transferFunds)
              .build();
          addTransactionToBankAccount(from, revert);
          throw new BankTransactionException("Couldn't complete the operation");
        }
      } else {
        throw new BankTransactionException("Couldn't complete the operation");
      }
    }
  }

  private boolean addTransactionToBankAccount(final BankAccount account, final BankOperation operation) {
    boolean transactionSuccessfull = false;
    try{
      transactionSuccessfull = account.addTransaction(operation);
      if(transactionSuccessfull == false) {
        throw new IllegalStateException();
      }
      
      return transactionSuccessfull;
    } catch (IllegalStateException e) {
      log.error("Failed to add credit/debit operation", e);
    }
    
    return transactionSuccessfull;
  }
}
