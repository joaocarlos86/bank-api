package com.revolut;

import java.util.stream.IntStream;

import com.revolut.controller.v1.BankAccountController;
import com.revolut.controller.v1.FundTransferController;
import com.revolut.domain.bank.Bank;
import com.revolut.domain.bank.BankAccount;
import com.revolut.service.exception.bank.BankAccountNotFoundException;
import com.revolut.service.exception.bank.BankTransactionException;
import com.revolut.service.exception.bank.InsufficientFundsException;
import com.revolut.service.exception.bank.InvalidTransferFundsException;
import com.revolut.service.exception.request.BadRequestException;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application {
  
  private static final int API_DEFAULT_PORT = 8080;
  private Javalin javalinApplication;
  
  public static void main(String[] args) {
    final Application application = new Application();
    application.start(API_DEFAULT_PORT);
  }

  public void start(final Integer port) {
    final Javalin app = Javalin
        .create(config -> {
          config.registerPlugin(new OpenApiPlugin(getOpenApiOptions()));
        })
        .start(port);

    app.get("/", ctx -> ctx.result("Hello Revolut"));
    app.get("/v1/bank/account/:bankAccountNumber", new BankAccountController().getBankAccount());
    app.post("/v1/transfer_funds/", new FundTransferController().submit());
    
    populateBankDataStore();
    configureExceptions(app);
    
    this.javalinApplication = app;
  }
  
  public void stop() {
    this.javalinApplication.stop();
  }

  private static OpenApiOptions getOpenApiOptions() {
    Info applicationInfo = new Info()
        .version("v1")
        .description("Fund Transfer - REVOLUT");
    return new OpenApiOptions(applicationInfo)
        .activateAnnotationScanningFor("com.revolut.controller.v1")
        .path("/swagger-docs")
        .swagger(new SwaggerOptions("/swagger").title("fund-transfer-api documentation"));
  }
  
  private static void configureExceptions(final Javalin app) {
    final String defaultErrorForLogStatement = "Failed to execute the operation";
    app.exception(BankAccountNotFoundException.class, (e, ctx) -> {
      log.error(defaultErrorForLogStatement, e);
      ctx.status(404).json(e);
    }).exception(BankTransactionException.class, (e, ctx) -> {
      log.error(defaultErrorForLogStatement, e);
      ctx.status(500).json(e);
    }).exception(InsufficientFundsException.class, (e, ctx) -> {
      log.error(defaultErrorForLogStatement, e);
      ctx.status(400).json(e);
    }).exception(InvalidTransferFundsException.class, (e, ctx) -> {
      log.error(defaultErrorForLogStatement, e);
      ctx.status(400).json(e);
    }).exception(BadRequestException.class, (e, ctx) -> {
      log.error(defaultErrorForLogStatement, e);
      ctx.status(400).json(e);
    }).exception(Exception.class, (e,ctx) -> {
      log.error(defaultErrorForLogStatement, e);
      ctx.status(500).json(e);
    });
  }

  private static void populateBankDataStore() {
    
    IntStream.range(1, 100000).forEach(accountNumber -> {
      final Double initialBalance = (Math.random() * 100000D);
      final String iban = String.format("ABC%s", accountNumber);
      final BankAccount bankAccount = new BankAccount(accountNumber, initialBalance, iban);
      Bank.getBank().addAccount(bankAccount);
      
    });
  }
}
