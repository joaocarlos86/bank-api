package com.revolut.controller.util;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.service.exception.request.MandatoryParameterNotSuppliedException;

import io.javalin.http.Context;

public class ContextParameterUtils {
  
  public String getMandatoryPathParameter(final Context context, final String parameterName) {
    final String parameter = Optional.ofNullable(context.pathParam(parameterName))
        .orElseThrow(() -> new MandatoryParameterNotSuppliedException(parameterName));
    
    if("".equals(parameter)) {
      throw new MandatoryParameterNotSuppliedException(parameterName);
    }
    
    return parameter;
  }
  
  public <T> T getObjectFromBody(Class<T> clazz, final Context context) throws IOException {
    
    final byte[] body = context.bodyAsBytes();
    return new ObjectMapper()
        .readValue(body, clazz);
  }
}
