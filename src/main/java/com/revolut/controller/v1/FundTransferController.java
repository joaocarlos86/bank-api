package com.revolut.controller.v1;

import java.math.BigDecimal;
import java.util.Optional;

import com.revolut.controller.util.ContextParameterUtils;
import com.revolut.domain.bank.Bank;
import com.revolut.domain.bank.BankAccount;
import com.revolut.domain.bank.dto.FundsTransferDTO;
import com.revolut.domain.bank.dto.FundsTransferResultDTO;
import com.revolut.service.BankService;
import com.revolut.service.exception.request.MandatoryParameterNotSuppliedException;
import com.revolut.service.operation.fundstransfer.FundsTransferOperation;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;

@OpenAPIDefinition
public class FundTransferController {

  private static final BankService BANK_SERVICE = new BankService();
  private ContextParameterUtils requestUtils;
  private Bank bank;

  public FundTransferController() {
    this.requestUtils = new ContextParameterUtils();
    this.bank = Bank.getBank();
  }
  
  public void setFormUtils(final ContextParameterUtils requestUtils) {
    this.requestUtils = requestUtils;
  }
  
  public void setBank(final Bank bank) {
    this.bank = bank;
  }

  @OpenApi(
      path = "/v1/transfer_funds/",
      method = HttpMethod.POST,
      description = "Funds transfer operation",
      operationId = "fundsTransfer",
      summary = "Receives a FundsTransferDTO in the body and returns FundsTrnasferOperation if the operation succeeds",
      tags = {"fundsTransfer"},
      requestBody = @OpenApiRequestBody(
          content = @OpenApiContent(from = FundsTransferDTO.class, isArray = false), 
          required = true, 
          description = "Basic data to complete the operation"),
      responses = {
          @OpenApiResponse(
              status = "200", 
              content = @OpenApiContent(from = FundsTransferOperation.class))
      }
  )
  public Handler submit() {
    return new Handler() {

      @Override
      public void handle(final Context ctx) throws Exception {
        
        final FundsTransferDTO dto = requestUtils.getObjectFromBody(FundsTransferDTO.class, ctx);
        
        final Optional<BankAccount> source = bank.getAccount(dto.getSourceBankAccountNumber());
        final Optional<BankAccount> target = bank.getAccount(dto.getTargetBankAccountNumber());
        final BigDecimal amount = BigDecimal.valueOf(dto.getAmount());
        
        final FundsTransferOperation fundTransfer = FundsTransferOperation
            .builder()
            .source(source.orElseThrow(() -> new MandatoryParameterNotSuppliedException("sourceBankAccountNumber")))
            .target(target.orElseThrow(() -> new MandatoryParameterNotSuppliedException("targetBankAccountNumber")))
            .amount(amount)
            .build();
        
        BANK_SERVICE.fundTransfer(fundTransfer);
        
        ctx.json(new FundsTransferResultDTO(fundTransfer));
      }
      
    };
  }
  
}
