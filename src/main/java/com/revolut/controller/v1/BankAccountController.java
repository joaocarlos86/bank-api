package com.revolut.controller.v1;

import com.revolut.controller.util.ContextParameterUtils;
import com.revolut.domain.bank.Bank;
import com.revolut.domain.bank.BankAccount;
import com.revolut.domain.bank.dto.BankAccountDTO;
import com.revolut.service.exception.bank.BankAccountNotFoundException;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiParam;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;

@OpenAPIDefinition
public class BankAccountController {

  private Bank bank;
  private ContextParameterUtils requestUtils;
  
  public BankAccountController() {
    this.bank = Bank.getBank();
    this.requestUtils = new ContextParameterUtils();
  }
  
  public void setBank(final Bank bank) {
    this.bank = bank;
  }
  
  public void setRequestUtils(ContextParameterUtils requestUtils) {
    this.requestUtils = requestUtils;
  }

  @OpenApi(
      path = "/v1/bank/account/:bankAccountNumber",
      method = HttpMethod.GET,
      description = "Get bank account details",
      operationId = "getBankAccount",
      summary = "Receives a bank account number in the as a path parameter and returns the bank account if found",
      tags = {"bankAccount"},
      pathParams = {
          @OpenApiParam(name = "bankAccountNumber", type = Integer.class, description = "Bank account number")
      },
      responses = {
          @OpenApiResponse(
              status = "200", 
              content = @OpenApiContent(from = BankAccountDTO.class))
      }
  )
  public Handler getBankAccount() {
    return new Handler() {
      @Override
      public void handle(Context ctx) throws Exception {
          final String bankAccountNumberAsString = requestUtils.getMandatoryPathParameter(ctx, "bankAccountNumber");
          final Integer bankAccountNumber = Integer.valueOf(bankAccountNumberAsString);
          final BankAccount bankAccount = bank
            .getAccount(bankAccountNumber)
            .orElseThrow(() -> new BankAccountNotFoundException(bankAccountNumber));
          ctx
            .json(
              new BankAccountDTO(bankAccount)
            );
      }
    };
  }

  
}
