# Fund Transfer API

Simple API to support fund transfer operations with support to parallelism.

## Getting Started

### Prerequisites

You will need:

```
Java 8
Apache Maven 3
```

### Installing

To install the application in your local maven repo, execute the following command in a terminal

```
mvn clean install
```

You should expect to see the following report at the end of the execution:
```
Tests run: 9, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.118 sec - in com.revolut.test.service.BankServiceTest
Running com.revolut.controller.v1.FundTransferControllerTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.267 sec - in com.revolut.controller.v1.FundTransferControllerTest
Running com.revolut.controller.v1.BankAccountControllerTest
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.009 sec - in com.revolut.controller.v1.BankAccountControllerTest

Results :

Tests run: 32, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ fund-transfer-api ---
[INFO] Building jar: ...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.912 s
[INFO] Finished at: 2019-08-30T18:45:50+01:00
[INFO] ------------------------------------------------------------------------
```

After running the command and given that the expected output was presented, the application will be installed in your local maven repository.

## Running the tests

To test the application, run the following command in the root directory of the application in a terminal window:

```
mvn clean test
```

You should expect to see the following report at the end of the execution:
```
Results :

Tests run: 32, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.545 s
[INFO] Finished at: 2019-08-30T18:50:10+01:00
[INFO] ------------------------------------------------------------------------

```

## Running the application

To run the application, simply run the following command in the root directory of the application in a terminal window:

```
mvn exec:java -Dexec.mainClass="com.revolut.Application"
```

You should expect to see the following output:

```
[com.revolut.Application.main()] INFO io.javalin.Javalin - 
           __                      __ _
          / /____ _ _   __ ____ _ / /(_)____
     __  / // __ `/| | / // __ `// // // __ \
    / /_/ // /_/ / | |/ // /_/ // // // / / /
    \____/ \__,_/  |___/ \__,_//_//_//_/ /_/

        https://javalin.io/documentation

[com.revolut.Application.main()] INFO org.eclipse.jetty.util.log - Logging initialized @1340ms to org.eclipse.jetty.util.log.Slf4jLog
[com.revolut.Application.main()] INFO io.javalin.Javalin - Starting Javalin ...
[com.revolut.Application.main()] INFO io.javalin.Javalin - Listening on http://localhost:8080/
[com.revolut.Application.main()] INFO io.javalin.Javalin - Javalin started in 192ms \o/
```

The API is now accessible on 
```
http://localhost:8080
```

Swagger is available at:
```
http://localhost:8080/swagger
```

OpenAPI docs are available at:
```
http://localhost:8080/swagger-docs
```

### Fund Transfer Endpoint:

Example:

```
curl -X POST \
  http://localhost:8080/v1/transfer_funds/ \
  -H 'Content-Type: application/json' \
  -d '{
	"sourceBankAccountNumber": 123,
	"targetBankAccountNumber": 456,
	"amount": 100.00
}'
```

### Bank Account Details Endpoint:

```
curl -X GET \
  http://localhost:8080/v1/bank/account/1
```

## Built With

* [Javelin](https://javalin.io) - A simple web framework for Java and Kotlin
* [Swagger](https://swagger.io) - Swagger aides in development across the entire API lifecycle, from design and documentation, to test and deployment.
* [OpenAPI](https://github.com/OAI/OpenAPI-Specification) - The OpenAPI Specification is a community-driven open specification within the OpenAPI Initiative, a Linux Foundation Collaborative Project.
* [Lombok](https://projectlombok.org) - Spicy Java
* [Maven](https://maven.apache.org/) - Dependency Management
* [Java](https://www.java.com) - Main runtime

## Authors

* **Joao Rodrigues** - *Initial work* - [joaocarlos86](https://github.com/joaocarlos86)